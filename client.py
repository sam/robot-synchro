import urllib.request

class Client:

    def __init__(self, server, robot):
        if server.endswith('/'):
            server = server[:-1]
        self.server = server
        self.robot = robot

    def _req(self, command):
        with urllib.request.urlopen(f"{self.server}/{command}/{self.robot}") as req:
            return f"{req.status} {req.reason}"

    def start(self):
        """Return True if this is my turn to start"""
        return self._req("START") == "204 OK"

    def ended(self):
        """Indicate that I have ended my turn. Return an exception
        if this was not my turn."""
        return self._req("ENDED")

    def ping(self):
        """Ping (not very useful, use start() instead)"""
        return self._req("PING")
