#! /usr/bin/env python
#
# Usage: server.py port
#
# For example: python3 server.py 8000

from http.server import BaseHTTPRequestHandler, HTTPServer
import time

alive = {}
start = 0
    
class HTTPRequestHandler(BaseHTTPRequestHandler):

    def do_GET(self):
        global start
        req = self.path[1:].split('/')
        robot = int(req[1])
        alive[robot] = time.time()
        if robot < 0 or robot > 4:
            self.send_response(303, "Permission denied: bad robot number")
        if req[0] == "PING":
            self.send_response(204, "Ok")
        elif req[0] == "ENDED" and robot == start:
            for i in range(1, 5):
                r = (robot + i) % 5
                if r in alive and time.time() - alive[r] < 10:
                    start = r
                    self.send_response(204, f"Next robot {r}")
            else:
                self.send_response(204, "No robot to start")
        elif req[0] == "ENDED":
            self.send_response(301, "This is not your turn")
        elif req[0] == "START":
            self.send_response(204, "OK" if robot == start else "NOT OK")
        elif req[0] == "RESET":
            start = robot
            self.send_response(204, f"Next robot {robot}")
        else:
            self.send_response(404, "Command not found")
        self.end_headers()
        
if __name__ == '__main__':
    import sys
    server = HTTPServer(('', int(sys.argv[1])), HTTPRequestHandler)
    try:
        server.serve_forever()
    except KeyboardInterrupt:
        pass
    server.server_close()
