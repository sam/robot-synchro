# Simple server and client for 5 robots synchronization

Everything uses `GET`. Robots are numbered from 0 to 4. They start in order,
skipping robots which have not been seen in the last 10 seconds.

## Server

By default, starts on port 8000, change port in `server.py` if needed.

### /RESET/n

Reset the game, mark robot `n` as the next one to start. To be used by the
game master, manually.

### /START/n

Return either "204 OK" if robot `n` is the one to start, or "204 Not
OK" otherwise.

### /ENDED/n

Declare that robot `n` has ended its turn. Next robot will receive the
authorization to start when it sends a `/START/` request.

### /PING/n

Declare that robot `n` is alive. Not strictly necessary, as any other
command (such as `/START/`) will also mark the robot as alive.

## Client

Create an instance of `client.Client` with the server base url and your
robot number (between 0 and 4). Then call methods `.start()` and `.ended()`
as needed.

### Example of client usage

```python
import client
import time

myself = client.Client("http://137.194.1.2:8000/", 3)  # I am robot 3

# Wait for my turn to start
while not myself.start():
    # Not my turn yet, ask half a second before asking again
    time.sleep(0.5)
    
# Do the race here
# (I won't write the code for you)

# Indicate that I have ended my turn
myself.ended()
```
